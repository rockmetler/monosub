﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MonoSub.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Account()
        {
            ViewBag.Message = "Dashboard";

            return View();
        }

        public ActionResult Support()
        {
            ViewBag.Message = "Support";

            return View();
        }
    }
}