﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MonoSub.Startup))]
namespace MonoSub
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
